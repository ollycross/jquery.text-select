# Text Select

A jQuery plugin to select, unselect and, if supported by the browser, copy text


## Installation

Install with NPM:

```npm install --save jquery.text-select```


## Usage

- Include jQuery
- Include jquery.text-select.js:

```
<script src="/path/to/jquery.js"></script>
<script src="/path/to/jquery.text-select.js"></script>
```

- Alternatively you can use webpack / browserify:
```
require('jquery.text-select');
```


```
<script>
  $(document).ready(function(){
    $("#selectMe").selectText(); // Hightlight / select the text
    $("#selectMe").selectText(false); // Clear the selection

    $("#copyMe").copyText(); // Copy text to clipboard


  });
</script>
```

## Methods

- `selectText(select)`: Selects the text; if `false` is passed unselects the text
- `copyText()`: Copies the text to clipboard, if supported by the browser (see notes below)

## Events

- `selecttext.selected`: When text is selected
- `selecttext.unselected`: When the selection is cleared
- `selecttext.copied`: When text is successfully copied to the clipboard

## Notes

This plugin is a jQuery wrapper for code presented by [https://stackoverflow.com/users/96100/tim-down](Tim Down), [https://stackoverflow.com/users/339850/gert-grenander](Gert Grenader) and [https://stackoverflow.com/users/406712/dean-taylor](Dean Taylor).

The plugin depends on `document.execCommand('copy')`, which according to MDN has the following support:
Browser | Support
--- | ---
Chrome | (Yes)
Edge | (Yes)
Firefox | 41
IE | 9
Opera | 29
Safari | 10

For security reasons, the command may only be executed following user interaction, so the event should be bound to an apprpriate handler (e.g. click).

For utility, the plugin attaches a boolean property `canCopyText` to the global jQuery object.  This will be true or false.  You can use this to check for browser support and show / hide copy buttons as appropriate.  If the `copyText` command is run without browser support no action will be taken and a warning logged to the console.